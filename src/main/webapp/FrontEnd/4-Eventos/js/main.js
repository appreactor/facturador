const mv = new Vue({
    el:'main',
    data: {
        nuevaTarea:null,
        tareas:[
            'Aprender Javascript',
            'Aprender Vue.js',
            'Rascarse la panza'
        ]
    },
    methods:{
        agregarTarea(){
            this.tareas.unshift(this.nuevaTarea);
            this.nuevaTarea = '';
        }
    }
});

// Evento con vanilla (JS puro)
function agregarTarea(){
    const campo = document.querySelector('input[type=text]');
    // unshift -> arreglos para agregar un nuevo objeto en la primera posicion 0
    // push -> ultima posicion
    mv.tareas.unshift(campo.value);
    campo.value = '';
}