const miVue = new Vue({
    el:'main',
    data:{
        laborales:['Lunes','Martes','Miercoles','Jueves','Viernes'],
        tareas:[
            {nombre:'Aprender Vue.js',prioridad:'alta'},
            {nombre:'Almorzar',prioridad:'media'},
            {nombre:'Aprender ES6',prioridad:'baja'}
        ],
        persona:{
            nombre:'Jorge',
            profesion:'Cobrador Gota a Gota',
            ciudad:'Bogotá D.C.'
        }
    }
});