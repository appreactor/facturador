// Usando vanilla (Javascript puro)

/*const texto = document.querySelector('input'); // document.getElementById('txtTexto');
const titulo = document.querySelector('h1');*/

// funcion tipo evento
/*texto.addEventListener('keyup', function(){
    titulo.innerHTML = texto.value;
});*/

// ES6 -> Funciones de Flecha
//texto.addEventListener('keyup', () => titulo.innerHTML = texto.value);

// vue.js
// Instancia de Vue
const miVue = new Vue({
    // Representa el elemento que vue va a contral
    el: 'main',
    // Modelo de datos que la instancia va a manejar
    data:{
        mensaje: 'Mensaje desde Vue'
    }
});