var form = document.querySelector('form');
var campoCorreo = document.getElementById('txtCorreo');

// evento de tecla para un campo de texto
campoCorreo.addEventListener('keyup',function(){
    console.log(campoCorreo.value);
});

// agregar un manejo de evento
form.addEventListener('submit',function(){
    var correo = document.getElementById('txtCorreo').value;
    var contrasena = document.getElementById('txtContrasena').value;

    alert('Correo: '+correo+' Contraseña: '+contrasena);
});