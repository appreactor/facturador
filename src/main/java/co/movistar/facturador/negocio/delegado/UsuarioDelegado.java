/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.delegado;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.negocio.util.CryptoUtil;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dao.UsuarioDao;
import co.movistar.facturador.persistencia.entidades.Usuario;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Lord_Nightmare
 */
public class UsuarioDelegado extends GenericoDelegado<Usuario>{
    
    private final UsuarioDao usuarioDao;
    
    public UsuarioDelegado(Connection cnn) {
        super(cnn);
        usuarioDao = new UsuarioDao(cnn);
        this.genericoDao = usuarioDao;
    }
    
    public Usuario consultarLogin(String correo, String contrasena)
            throws FacturadorException{
        try {
            /**
             * Validacion de correo electronico por expresion regular
             */
            Usuario autenticado = usuarioDao.consultar(
                    correo, 
                    CryptoUtil.cifrarContrasena(contrasena)
            );
            // validar la existencia del objeto del usuario autenticado
            if (autenticado.getIdUsuario() == null) {
                //disparar una excepcion de no resultados
                throw new FacturadorException(EMensajes.NO_RESULTADOS);
            }
            return autenticado;
        } catch (FacturadorException | SQLException e) {
            e.printStackTrace(System.err);
            Conexion.deshacer(cnn);
            if (e instanceof FacturadorException) {
                throw (FacturadorException) e;
            } else{
                throw new FacturadorException(EMensajes.ERROR_CONSULTAR);
            }
        }
    }
    
}
