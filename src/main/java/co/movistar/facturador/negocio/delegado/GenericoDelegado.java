/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.delegado;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dao.crud.IGenericoDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lord_Nightmare
 */
public abstract class GenericoDelegado<T> {
    
    protected Connection cnn;
    protected IGenericoDao genericoDao;
    // protected boolan confirmar

    public GenericoDelegado(Connection cnn) {
        this.cnn = cnn;
    }
    
    public void insertar(T entidad) throws FacturadorException{
        try {
            genericoDao.insertar(entidad);
        } catch (SQLException e) {
            Conexion.deshacer(cnn);
            e.printStackTrace();
            throw new FacturadorException(EMensajes.ERROR_INSERTAR);
        }
    }
    
    public void editar(T entidad) throws FacturadorException{
        try {
            genericoDao.editar(entidad);
        } catch (SQLException e) {
            Conexion.deshacer(cnn);
            e.printStackTrace();
            throw new FacturadorException(EMensajes.ERROR_MODIFICAR);
        }
    }
    
    public List<T> consultar() throws FacturadorException {
        try {
            List lista = genericoDao.consultar();
            if (lista.isEmpty()) {
                throw new FacturadorException(EMensajes.NO_RESULTADOS);
            }
            return lista;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FacturadorException(EMensajes.ERROR_CONSULTAR);
        }
    }
    
    public T consultar(long id) throws FacturadorException{
        try {
            return (T) genericoDao.consultar(id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FacturadorException(EMensajes.ERROR_CONSULTAR);
        }
    }
    
}
