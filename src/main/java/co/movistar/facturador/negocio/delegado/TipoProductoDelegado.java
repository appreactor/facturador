/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.delegado;

import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dao.TipoProductoDao;
import co.movistar.facturador.persistencia.entidades.TipoProducto;
import java.sql.Connection;

/**
 *
 * @author Lord_Nightmare
 */
public class TipoProductoDelegado extends GenericoDelegado<TipoProducto> {

    private final TipoProductoDao tipoProductoDao;

    public TipoProductoDelegado(Connection cnn) {
        super(cnn);
        this.tipoProductoDao = new TipoProductoDao(cnn);
        genericoDao = this.tipoProductoDao;
    }

    public void guardarTipoProducto(TipoProducto tipoProducto)
            throws FacturadorException {
        // Vamos a validar si es un objeto nuevo o un objeto registrado
        if (tipoProducto.getIdTipoProducto() != null) {
            super.editar(tipoProducto);
        } else {
            super.insertar(tipoProducto);
        }
        Conexion.confirmar(cnn);
        Conexion.desconectar(cnn);
    }

}
