/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.delegado;

import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dao.RolDao;
import co.movistar.facturador.persistencia.entidades.Rol;
import java.sql.Connection;

/**
 *
 * @author Lord_Nightmare
 */
public class RolDelegado extends GenericoDelegado<Rol> {

    private final RolDao rolDao;

    public RolDelegado(Connection cnn) {
        super(cnn);
        rolDao = new RolDao(cnn);
        this.genericoDao = rolDao;
    }

    public void guardarRol(Rol rol) throws FacturadorException {
        insertar(rol);
        Conexion.confirmar(cnn);
    }

}
