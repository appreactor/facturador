/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.constantes;

/**
 *
 * @author Lord_Nightmare
 */
public enum EMensajes {

    INSERTO(1, "Se ha insertado correctamente"),
    MODIFICO(1, "Se ha modificado correctamente"),
    CONSULTO(1, "Se ha consultado correctamente"),
    NO_RESULTADOS(0, "La consulta no arrojo resultados"),
    ERROR_CONEXION_BD(-2,
            "No se ha podido establecer conexion con la base de datos"),
    ERROR_INSERTAR(-1, "Error al insertar el registro"),
    ERROR_MODIFICAR(-1, "Error al modificar el registro"),
    ERROR_CONSULTAR(-1, "Error al consultar los datos"),
    ERROR_CIFRADO_CONTRASENA(-1, "Error al leer la contraseña"),
    ERROR_GENERACION_TOKEN(-3, "El token de acceso no ha sido generado"), 
    ERROR_EXPIRACION_TOKEN(-3, "La fecha de validez del token ha caducado"),
    ERROR_TOKEN_CORRUPTO(-3, "El token que intenta enviar, ha sido manipulado");

    private int codigo;
    private String mensaje;

    private EMensajes(int codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
