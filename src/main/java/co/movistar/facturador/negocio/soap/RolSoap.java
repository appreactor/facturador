/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.soap;

import co.movistar.facturador.negocio.delegado.RolDelegado;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.entidades.Rol;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Lord_Nightmare
 */
@WebService(serviceName = "RolSoap")
public class RolSoap {

    /**
     * This is a sample web service operation
     *
     * @param txt
     * @return
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    @WebMethod(operationName = "guardarRol")
    public Rol guardarRol(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "estado") Boolean estado)
            throws FacturadorException {
        Rol nuevoRol = new Rol();
        nuevoRol.setNombre(nombre);
        nuevoRol.setEstado(estado);
        new RolDelegado(Conexion.conectar()).guardarRol(nuevoRol);
        return nuevoRol;
    }
}
