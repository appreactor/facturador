/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.excepciones;

import co.movistar.facturador.negocio.constantes.EMensajes;

/**
 *
 * @author Lord_Nightmare
 */
public class FacturadorException extends Exception{
    
    /**
     * Codigo de la excepcion a disparar
     */
    private int codigo;
    
    /**
     * Descripcion textual de la excepcion
     */
    private String descripcion;
    
    /**
     * Datos adicionales para enviar al cliente sobre la excepcion
     */
    private Object datos;

    /**
     * Constructor implicito de la clase
     */
    public FacturadorException() {
    }

    /**
     * Constructor sobrecargado con codigo y descripcion de la excepcion
     * 
     * @param mensaje Representacion de un mensaje para manejo de excepciones
     * contiene el codigo del error y su respectivo mensaje
     */
    public FacturadorException(EMensajes mensaje) {
        this.codigo = mensaje.getCodigo();
        this.descripcion = mensaje.getMensaje();
    }

    public FacturadorException(EMensajes mensaje, Object datos) {
        this.codigo = mensaje.getCodigo();
        this.descripcion = mensaje.getMensaje();
        this.datos = datos;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the datos
     */
    public Object getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(Object datos) {
        this.datos = datos;
    }
    
    
    
}
