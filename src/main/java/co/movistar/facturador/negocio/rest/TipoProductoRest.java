/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.rest;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.constantes.Secured;
import co.movistar.facturador.negocio.delegado.TipoProductoDelegado;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dto.RespuestaDto;
import co.movistar.facturador.persistencia.entidades.TipoProducto;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Secured
@Path("tipo-producto")
public class TipoProductoRest {

    @Context
    private UriInfo context;
    
    @Context
    private SecurityContext securityContext;

    /**
     * Creates a new instance of TipoProductoRest
     */
    public TipoProductoRest() {
    }

    @POST
    @Path("guardar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaDto insertarTipoProducto(TipoProducto tipoProducto) {
        RespuestaDto respuesta;
        try {
            new TipoProductoDelegado(Conexion.conectar())
                    .guardarTipoProducto(tipoProducto);
            respuesta = new RespuestaDto(EMensajes.INSERTO);
            respuesta.setDatos(securityContext.getUserPrincipal());
        } catch (FacturadorException e) {
            respuesta = new RespuestaDto(e);
        }
        return respuesta;
    }
    
    @GET
    @Path("consultar")
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaDto consultar(){
        RespuestaDto respuesta;
        try {
            respuesta = new RespuestaDto(EMensajes.CONSULTO);
            respuesta.setDatos(new TipoProductoDelegado(Conexion.conectar()).consultar());
        } catch (FacturadorException e) {
            respuesta = new RespuestaDto(e);
        }
        return respuesta;
    }
    
    @GET
    @Path("buscar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaDto buscar(@PathParam("id") long id){
        RespuestaDto respuesta;
        try {
            respuesta = new RespuestaDto(EMensajes.CONSULTO);
            respuesta.setDatos(new TipoProductoDelegado(Conexion.conectar()).consultar(id));
        } catch (FacturadorException e) {
            respuesta = new RespuestaDto(e);
        }
        return respuesta;
    }
}
