/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.rest.autoridad;

import co.movistar.facturador.persistencia.entidades.Usuario;
import java.security.Principal;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Lord_Nightmare
 */
public class FacturadorSecurityContext implements SecurityContext{
    
    /**
     * Representa la entidad con la que se maneja la autenticacion
     */
    private Usuario usuario;
    
    /**
     * Si el servidor esta trabajando con el protocolo http | https
     */
    private String esquema;

    public FacturadorSecurityContext(Usuario usuario, String esquema) {
        this.usuario = usuario;
        this.esquema = esquema;
    }
    
    @Override
    public Principal getUserPrincipal() {
        return this.usuario;
    }

    @Override
    public boolean isUserInRole(String role) {
        return true;
    }

    @Override
    public boolean isSecure() {
        return this.esquema.equals("https");
    }

    @Override
    public String getAuthenticationScheme() {
        return "Token-Based-Auth-Scheme";
    }
    
}
