/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.rest.filtros;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.constantes.Secured;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.negocio.rest.autoridad.FacturadorSecurityContext;
import co.movistar.facturador.negocio.util.TokenUtil;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.entidades.Usuario;
import java.sql.Connection;
import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Lord_Nightmare
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AutenticacionFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {

        // obtener el encabezado de la peticion http|https que llega al servidor
        String encabezadoAuth = requestContext.getHeaderString(
                HttpHeaders.AUTHORIZATION
        );

        // Verificar si el encabezado HTTP Authorization exista, tenga el formato
        // adecuado
        if (encabezadoAuth == null || !encabezadoAuth.startsWith("Bearer ")) {
            throw new NotAuthorizedException("El encabezado de autorizacion debe"
                    + " ser proporcionado.");
        }

        // extraer el valor real del token de autorizacion
        String token = encabezadoAuth.substring("Bearer".length()).trim();
        Connection cnn = null;
        try {
            cnn = Conexion.conectar();
            // obtener el usuario de la base de datos segun el token
            Usuario usuario = TokenUtil.validarToken(token, cnn);
            if (usuario.getIdUsuario() == null) {
                throw new FacturadorException(EMensajes.ERROR_CONSULTAR);
            }
            // Agregar el usuario al contexto de seguridad para que los 
            // web services tengan acceso a la informacion de dicho usuario
            requestContext.setSecurityContext(
                    new FacturadorSecurityContext(
                            usuario,
                            requestContext.getUriInfo().getRequestUri().getScheme()
                    )
            );
        } catch (FacturadorException e) {
            // retornar el codigo HTTP de No Autorizado
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        } finally {
            Conexion.desconectar(cnn);
        }
    }

}
