/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.rest.autoridad;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.delegado.UsuarioDelegado;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.negocio.util.TokenUtil;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dto.RespuestaDto;
import co.movistar.facturador.persistencia.entidades.Usuario;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("login")
public class AutorizacionRest {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AutorizacionRest
     */
    public AutorizacionRest() {
    }

    /**
     * Retrieves representation of an instance of co.movistar.facturador.negocio.rest.autoridad.AutorizacionRest
     * @return an instance of co.movistar.facturador.persistencia.dto.RespuestaDto
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaDto autenticarUsuario(
            @FormParam("correo") String correo,
            @FormParam("contrasena") String contrasena) {
        RespuestaDto respuesta;
        try {
            Usuario usuario = new UsuarioDelegado(Conexion.conectar()).consultarLogin(correo, contrasena);
            String token = TokenUtil.generarToken(usuario);
            respuesta = new RespuestaDto(EMensajes.CONSULTO);
            respuesta.setDatos(token);
        } catch (FacturadorException e) {
            respuesta = new RespuestaDto();
            respuesta.setCodigo(e.getCodigo());
            respuesta.setMensaje(e.getDescripcion());
        }
        return respuesta;
    }
}
