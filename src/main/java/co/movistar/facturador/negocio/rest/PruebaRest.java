/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.rest;

import co.movistar.facturador.persistencia.entidades.Rol;
import co.movistar.facturador.persistencia.entidades.Usuario;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("prueba")
public class PruebaRest {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PruebaRest
     */
    public PruebaRest() {
    }

    /**
     * Retrieves representation of an instance of co.movistar.facturador.negocio.rest.PruebaRest
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getJson() {
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(1l);
        usuario.setNombre("Gerardo Perez");
        usuario.setRol(new Rol(23L));
        usuario.setCorreo("");
        return usuario;
    }

    /**
     * PUT method for updating or creating an instance of PruebaRest
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
