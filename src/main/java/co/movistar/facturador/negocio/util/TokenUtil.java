/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.util;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.delegado.UsuarioDelegado;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import co.movistar.facturador.persistencia.entidades.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Lord_Nightmare
 */
public final class TokenUtil {

    /**
     * Llave secreta de firmado del token
     */
    private final static String LLAVE = "FaCtUrAdOr";

    public static String generarToken(Usuario usuario)
            throws FacturadorException {
        String token;
        try {
            /**
             * fecha actual del servidor
             */
            LocalDateTime fecha = LocalDateTime.now();
            token = Jwts.builder()
                    .setSubject(String.valueOf(new Random().nextInt(2000000)))
                    .setId(String.valueOf(usuario.getIdUsuario()))
                    .setIssuedAt(
                            Date.from(
                                    fecha.atZone(
                                            ZoneId.systemDefault()
                                    ).toInstant()
                            )
                    )
                    .setExpiration(
                            Date.from(
                                    fecha.plusMinutes(30).atZone(
                                            ZoneId.systemDefault()
                                    ).toInstant()
                            )
                    )
                    .claim("correo", usuario.getCorreo())
                    .claim("nombre", usuario.getNombre())
                    .signWith(SignatureAlgorithm.HS256, LLAVE.getBytes("UTF-8"))
                    .compact();
        } catch (UnsupportedEncodingException e) {
            throw new FacturadorException(EMensajes.ERROR_GENERACION_TOKEN);
        }
        return "Bearer " + token;
    }
    
    public static Usuario validarToken(String token, Connection cnn)
            throws FacturadorException{
        try {
            // obtener decifrada el toke basados en el string del parametro token
            Jws<Claims> tokenDecifrado = Jwts.parser()
                    .setSigningKey(LLAVE.getBytes("UTF-8"))
                    .parseClaimsJws(token);
            
            // java.util.Date | java.time.LocalDateTime
            LocalDateTime fecha = DateUtil.parseLocalDateTime(
                    tokenDecifrado.getBody().getExpiration()
            );
            
            if (fecha.isBefore(LocalDateTime.now())) {
                // La fecha de vigencia del token ha caducado
                throw new FacturadorException(EMensajes.ERROR_EXPIRACION_TOKEN);
            }
            return new UsuarioDelegado(cnn).consultar(Long.parseLong(tokenDecifrado.getBody().getId()));
        } catch (ExpiredJwtException | MalformedJwtException 
                | SignatureException | UnsupportedJwtException 
                | UnsupportedEncodingException | IllegalArgumentException e) {
            throw new FacturadorException(EMensajes.ERROR_TOKEN_CORRUPTO);
        }
    }
}
