package co.movistar.facturador.negocio.util;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 *
 * @author Fabian
 */
public class DateUtil {

    public static java.sql.Date parseDate(java.util.Date fecha) {
        if (fecha == null) {
            return null;
        }
        return new java.sql.Date(fecha.getTime());
    }

    public static java.sql.Timestamp parseTimestamp(java.util.Date fecha) {
        if (fecha == null) {
            return null;
        }
        return new java.sql.Timestamp(fecha.getTime());
    }
    
    /**
     * Metodo que convierte una fecha clasica <code>java.util.Date</code>
     * a una instancia de <code>java.time.LocalDateTime</code> para realizar 
     * operaciones con fechas de una manera mas sencilla
     * @param fecha instacia de un java.util.Date
     * @return instancia de un java.time.LocalDateTime
     */
    public static LocalDateTime parseLocalDateTime(java.util.Date fecha){
        if (fecha == null) {
            return LocalDateTime.now();
        }
        return fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
