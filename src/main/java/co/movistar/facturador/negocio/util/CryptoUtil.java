/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.negocio.util;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Lord_Nightmare
 */
public final class CryptoUtil {

    /**
     * Valor de plantilla para la contraseña autogenerada en la creacion de
     * usuarios
     */
    private static final String CONTRASENA_DEFECTO = "Abcd123456!";

    /**
     * Valor del cifrado de primer nivel
     */
    private static final String SHA_256 = "SHA-256";

    /**
     * Valor del cifrado de segundo nivel
     */
    private static final String SHA_512 = "SHA-512";

    /**
     * Permite transformar una cadena de texto a un valor cifrado asimetricamente
     * @param texto texto a cifrar
     * @param tipo el formato del algoritmo asimetrico
     * @return hash o cadena de texto cifrada
     * @throws NoSuchAlgorithmException en caso de que el valor del formato de
     * cifrado este escrito de manera incorrecta
     */
    private static String cifrar(String texto, String tipo)
            throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(tipo);
        // Actualizar el valor de texto del md con el valor que deseamos cifrar
        md.update(texto.getBytes());

        // Obtenemos los bytes cifrados por el md
        byte[] data = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            sb.append(
                    Integer.toString(
                            (data[i] & 0xff) + 0x100, 16
                    ).substring(1)
            );
        }
        return sb.toString();
    }
    
    public static String cifrarContrasena(String contrasena)
            throws FacturadorException{
        String contrasenaCifrada;
        try {
            contrasenaCifrada = cifrar(contrasena, SHA_512);
        } catch (NoSuchAlgorithmException e) {
            throw new FacturadorException(EMensajes.ERROR_CIFRADO_CONTRASENA);
        }
        return contrasenaCifrada;
    }

}
