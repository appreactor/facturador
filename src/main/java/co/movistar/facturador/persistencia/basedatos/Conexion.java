/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.persistencia.basedatos;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Lord_Nightmare
 */
public final class Conexion {
    
    public static Connection conectar() throws FacturadorException{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            /*Connection cnn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/facturador_db"
                    , "root"
                    , "xy36bic92"
            );*/
            // Conexion via Pool
            Context ctx = new InitialContext();
            // obtener el datasource
            DataSource ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/FacturadorDs");
            
            Connection cnn = ds.getConnection();
            // facultar a la conexion para que no haga autocommit
            cnn.setAutoCommit(false);
            return cnn;
        } catch (NamingException | ClassNotFoundException | SQLException ex) {
            throw new FacturadorException(EMensajes.ERROR_CONEXION_BD);
        }
    }
    
    public static void desconectar(Connection cnn){
        desconectar(cnn,null);
    }
    
    public static void desconectar(PreparedStatement ps){
        desconectar(null, ps);
    }
    
    private static void desconectar(Connection cnn, PreparedStatement ps){
        try {
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }
    
    public static void confirmar(Connection cnn){
        try {
            cnn.commit();
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }
    
    public static void deshacer(Connection cnn){
        try {
            cnn.rollback();
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }
}
