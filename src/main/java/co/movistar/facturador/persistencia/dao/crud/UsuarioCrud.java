package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import co.movistar.facturador.persistencia.entidades.Rol;
import co.movistar.facturador.persistencia.entidades.Usuario;

public class UsuarioCrud implements IGenericoDao<Usuario> {

    protected final int ID = 1;
    protected Connection cnn;

    public UsuarioCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Usuario usuario) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into usuario(nombre,documento,telefono,direccion,correo,contrasena,estado,id_rol) values (?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, usuario.getNombre());
            sentencia.setObject(i++, usuario.getDocumento());
            sentencia.setObject(i++, usuario.getTelefono());
            sentencia.setObject(i++, usuario.getDireccion());
            sentencia.setObject(i++, usuario.getCorreo());
            sentencia.setObject(i++, usuario.getContrasena());
            sentencia.setObject(i++, usuario.getEstado());
            sentencia.setObject(i++, usuario.getRol().getIdRol());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                usuario.setIdUsuario(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Usuario usuario) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update usuario set nombre=?,documento=?,telefono=?,direccion=?,correo=?,contrasena=?,estado=?,id_rol=? where id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, usuario.getNombre());
            sentencia.setObject(i++, usuario.getDocumento());
            sentencia.setObject(i++, usuario.getTelefono());
            sentencia.setObject(i++, usuario.getDireccion());
            sentencia.setObject(i++, usuario.getCorreo());
            sentencia.setObject(i++, usuario.getContrasena());
            sentencia.setObject(i++, usuario.getEstado());
            sentencia.setObject(i++, usuario.getRol().getIdRol());
            sentencia.setObject(i++, usuario.getIdUsuario());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Usuario> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Usuario> lista = new ArrayList<>();
        try {

            String sql = "select * from usuario";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getUsuario(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Usuario consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Usuario obj = null;
        try {

            String sql = "select * from usuario where id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getUsuario(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static Usuario getUsuario(ResultSet rs) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(rs.getLong("id_usuario"));
        usuario.setNombre(rs.getString("nombre"));
        usuario.setDocumento(rs.getLong("documento"));
        usuario.setTelefono(rs.getString("telefono"));
        usuario.setDireccion(rs.getString("direccion"));
        usuario.setCorreo(rs.getString("correo"));
        usuario.setContrasena(rs.getString("contrasena"));
        usuario.setEstado(rs.getBoolean("estado"));
        usuario.setRol(new Rol(rs.getLong("id_rol")));
        return usuario;
    }
}
