package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.TipoProductoCrud;
import java.sql.Connection;

public class TipoProductoDao extends TipoProductoCrud {

  public TipoProductoDao(Connection cnn){
    super(cnn);
  }
}
