package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.entidades.Cliente;
import co.movistar.facturador.persistencia.entidades.TipoCliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;

public class ClienteCrud implements IGenericoDao<Cliente> {

    protected final int ID = 1;
    protected Connection cnn;

    public ClienteCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Cliente cliente) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into cliente(nombre,correo,telefono,direccion,estado,id_tipo_cliente) values (?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, cliente.getNombre());
            sentencia.setObject(i++, cliente.getCorreo());
            sentencia.setObject(i++, cliente.getTelefono());
            sentencia.setObject(i++, cliente.getDireccion());
            sentencia.setObject(i++, cliente.getEstado());
            sentencia.setObject(i++, cliente.getTipoCliente().getIdTipoCliente());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                cliente.setIdCliente(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Cliente cliente) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update cliente set nombre=?,correo=?,telefono=?,direccion=?,estado=?,id_tipo_cliente=? where id_cliente=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, cliente.getNombre());
            sentencia.setObject(i++, cliente.getCorreo());
            sentencia.setObject(i++, cliente.getTelefono());
            sentencia.setObject(i++, cliente.getDireccion());
            sentencia.setObject(i++, cliente.getEstado());
            sentencia.setObject(i++, cliente.getTipoCliente().getIdTipoCliente());
            sentencia.setObject(i++, cliente.getIdCliente());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Cliente> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Cliente> lista = new ArrayList<>();
        try {

            String sql = "select * from cliente";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getCliente(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Cliente consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Cliente obj = null;
        try {

            String sql = "select * from cliente where id_cliente=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getCliente(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static Cliente getCliente(ResultSet rs) throws SQLException {
        Cliente cliente = new Cliente();
        cliente.setIdCliente(rs.getLong("id_cliente"));
        cliente.setNombre(rs.getString("nombre"));
        cliente.setCorreo(rs.getString("correo"));
        cliente.setTelefono(rs.getString("telefono"));
        cliente.setDireccion(rs.getString("direccion"));
        cliente.setEstado(rs.getBoolean("estado"));
        cliente.setTipoCliente(new TipoCliente(rs.getLong("id_tipo_cliente")));
        return cliente;
    }

}
