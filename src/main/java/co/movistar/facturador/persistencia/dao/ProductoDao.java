package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.ProductoCrud;
import java.sql.Connection;
public class ProductoDao extends ProductoCrud {

  public ProductoDao(Connection cnn){
    super(cnn);
  }
}
