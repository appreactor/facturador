package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.negocio.util.DateUtil;
import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import co.movistar.facturador.persistencia.entidades.Cliente;
import co.movistar.facturador.persistencia.entidades.Factura;
import co.movistar.facturador.persistencia.entidades.Usuario;

public class FacturaCrud implements IGenericoDao<Factura> {

    protected final int ID = 1;
    protected Connection cnn;

    public FacturaCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Factura factura) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into factura(fecha_generacion,fecha_pago,fecha_garantia,estado,id_cliente,id_usuario) values (?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaGeneracion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaPago()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaGarantia()));
            sentencia.setObject(i++, factura.getEstado());
            sentencia.setObject(i++, factura.getCliente().getIdCliente());
            sentencia.setObject(i++, factura.getUsuario().getIdUsuario());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                factura.setIdFactura(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Factura factura) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update factura set fecha_generacion=?,fecha_pago=?,fecha_garantia=?,estado=?,id_cliente=?,id_usuario=? where id_factura=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaGeneracion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaPago()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(factura.getFechaGarantia()));
            sentencia.setObject(i++, factura.getEstado());
            sentencia.setObject(i++, factura.getCliente().getIdCliente());
            sentencia.setObject(i++, factura.getUsuario().getIdUsuario());
            sentencia.setObject(i++, factura.getIdFactura());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Factura> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Factura> lista = new ArrayList<>();
        try {

            String sql = "select * from factura";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getFactura(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Factura consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Factura obj = null;
        try {

            String sql = "select * from factura where id_factura=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getFactura(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static Factura getFactura(ResultSet rs) throws SQLException {
        Factura factura = new Factura();
        factura.setIdFactura(rs.getLong("id_factura"));
        factura.setFechaGeneracion(rs.getTimestamp("fecha_generacion"));
        factura.setFechaPago(rs.getTimestamp("fecha_pago"));
        factura.setFechaGarantia(rs.getTimestamp("fecha_garantia"));
        factura.setEstado(rs.getBoolean("estado"));
        factura.setCliente(new Cliente(rs.getLong("id_cliente")));
        factura.setUsuario(new Usuario(rs.getLong("id_usuario")));

        return factura;
    }
}
