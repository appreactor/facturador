package co.movistar.facturador.persistencia.dao;


import co.movistar.facturador.persistencia.basedatos.Conexion;
import co.movistar.facturador.persistencia.dao.crud.UsuarioCrud;
import static co.movistar.facturador.persistencia.dao.crud.UsuarioCrud.getUsuario;
import co.movistar.facturador.persistencia.entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioDao extends UsuarioCrud {

    public UsuarioDao(Connection cnn) {
        super(cnn);
    }
    
    public Usuario consultar(String correo, String contrasena) throws SQLException {
        PreparedStatement sentencia = null;
        Usuario obj = new Usuario();
        try {
            int i = 1;
            String sql = "select * from usuario where correo=? and contrasena=? and estado=1";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, correo);
            sentencia.setObject(i++, contrasena);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getUsuario(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

}
