package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.facturador.persistencia.dao.TipoProductoDao;
import co.movistar.facturador.persistencia.entidades.Producto;
import co.movistar.facturador.persistencia.entidades.TipoProducto;

public class ProductoCrud implements IGenericoDao<Producto> {

    protected final int ID = 1;
    protected Connection cnn;

    public ProductoCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Producto producto) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into producto(nombre,precio_unitario,cantidad,estado,id_tipo_producto) values (?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, producto.getNombre());
            sentencia.setObject(i++, producto.getPrecioUnitario());
            sentencia.setObject(i++, producto.getCantidad());
            sentencia.setObject(i++, producto.getEstado());
            sentencia.setObject(i++, producto.getTipoProducto().getIdTipoProducto());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                producto.setIdProducto(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Producto producto) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update producto set nombre=?,precio_unitario=?,cantidad=?,estado=?,id_tipo_producto=? where id_producto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, producto.getNombre());
            sentencia.setObject(i++, producto.getPrecioUnitario());
            sentencia.setObject(i++, producto.getCantidad());
            sentencia.setObject(i++, producto.getEstado());
            sentencia.setObject(i++, producto.getTipoProducto().getIdTipoProducto());
            sentencia.setObject(i++, producto.getIdProducto());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Producto> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Producto> lista = new ArrayList<>();
        try {

            String sql = "select * from producto";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getProducto(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Producto consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Producto obj = null;
        try {

            String sql = "select * from producto where id_producto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getProducto(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static Producto getProducto(ResultSet rs) throws SQLException {
        Producto producto = new Producto();
        producto.setIdProducto(rs.getLong("id_producto"));
        producto.setNombre(rs.getString("nombre"));
        producto.setPrecioUnitario(rs.getLong("precio_unitario"));
        producto.setCantidad(rs.getLong("cantidad"));
        producto.setEstado(rs.getBoolean("estado"));
        producto.setTipoProducto(new TipoProducto(rs.getLong("id_tipo_producto")));
        return producto;
    }

}
