package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.TipoClienteCrud;
import java.sql.Connection;

public class TipoClienteDao extends TipoClienteCrud {

    public TipoClienteDao(Connection cnn) {
        super(cnn);
    }
}
