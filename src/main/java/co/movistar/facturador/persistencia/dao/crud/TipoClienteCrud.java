package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.facturador.persistencia.entidades.TipoCliente;

public class TipoClienteCrud implements IGenericoDao<TipoCliente> {

    protected final int ID = 1;
    protected Connection cnn;

    public TipoClienteCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(TipoCliente tipoCliente) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into tipo_cliente(nombre,estado) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, tipoCliente.getNombre());
            sentencia.setObject(i++, tipoCliente.getEstado());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                tipoCliente.setIdTipoCliente(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(TipoCliente tipoCliente) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update tipo_cliente set nombre=?,estado=? where id_tipo_cliente=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, tipoCliente.getNombre());
            sentencia.setObject(i++, tipoCliente.getEstado());
            sentencia.setObject(i++, tipoCliente.getIdTipoCliente());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<TipoCliente> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<TipoCliente> lista = new ArrayList<>();
        try {

            String sql = "select * from tipo_cliente";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getTipoCliente(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public TipoCliente consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        TipoCliente obj = null;
        try {

            String sql = "select * from tipo_cliente where id_tipo_cliente=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getTipoCliente(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static TipoCliente getTipoCliente(ResultSet rs) throws SQLException {
        TipoCliente tipoCliente = new TipoCliente();
        tipoCliente.setIdTipoCliente(rs.getLong("id_tipo_cliente"));
        tipoCliente.setNombre(rs.getString("nombre"));
        tipoCliente.setEstado(rs.getBoolean("estado"));

        return tipoCliente;
    }

}
