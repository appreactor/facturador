package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.FacturaCrud;
import java.sql.Connection;


public class FacturaDao extends FacturaCrud {

  public FacturaDao(Connection cnn){
    super(cnn);
  }
}
