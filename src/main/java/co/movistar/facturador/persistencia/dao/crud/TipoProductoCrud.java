package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import co.movistar.facturador.persistencia.entidades.TipoProducto;

public class TipoProductoCrud implements IGenericoDao<TipoProducto> {

    protected final int ID = 1;
    protected Connection cnn;

    public TipoProductoCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(TipoProducto tipoProducto) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into tipo_producto(nombre,estado) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, tipoProducto.getNombre());
            sentencia.setObject(i++, tipoProducto.getEstado());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                tipoProducto.setIdTipoProducto(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(TipoProducto tipoProducto) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update tipo_producto set nombre=?,estado=? where id_tipo_producto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, tipoProducto.getNombre());
            sentencia.setObject(i++, tipoProducto.getEstado());
            sentencia.setObject(i++, tipoProducto.getIdTipoProducto());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<TipoProducto> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<TipoProducto> lista = new ArrayList<>();
        try {

            String sql = "select * from tipo_producto";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getTipoProducto(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public TipoProducto consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        TipoProducto obj = null;
        try {

            String sql = "select * from tipo_producto where id_tipo_producto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getTipoProducto(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static TipoProducto getTipoProducto(ResultSet rs) throws SQLException {
        TipoProducto tipoProducto = new TipoProducto();
        tipoProducto.setIdTipoProducto(rs.getLong("id_tipo_producto"));
        tipoProducto.setNombre(rs.getString("nombre"));
        tipoProducto.setEstado(rs.getBoolean("estado"));
        return tipoProducto;
    }

}
