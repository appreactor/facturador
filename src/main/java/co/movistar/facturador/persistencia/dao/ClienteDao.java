package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.ClienteCrud;
import java.sql.Connection;


public class ClienteDao extends ClienteCrud {

  public ClienteDao(Connection cnn){
    super(cnn);
  }
}
