package co.movistar.facturador.persistencia.dao;

import co.movistar.facturador.persistencia.dao.crud.DetalleFacturaCrud;
import java.sql.Connection;


public class DetalleFacturaDao extends DetalleFacturaCrud {

  public DetalleFacturaDao(Connection cnn){
    super(cnn);
  }
}
