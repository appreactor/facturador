package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import co.movistar.facturador.persistencia.entidades.DetalleFactura;
import co.movistar.facturador.persistencia.entidades.Factura;
import co.movistar.facturador.persistencia.entidades.Producto;

public class DetalleFacturaCrud implements IGenericoDao<DetalleFactura> {

    protected final int ID = 1;
    protected Connection cnn;

    public DetalleFacturaCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(DetalleFactura detalleFactura) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into detalle_factura(cantidad,valor_parcial,id_producto,id_factura) values (?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, detalleFactura.getCantidad());
            sentencia.setObject(i++, detalleFactura.getValorParcial());
            sentencia.setObject(i++, detalleFactura.getProducto().getIdProducto());
            sentencia.setObject(i++, detalleFactura.getFactura().getIdFactura());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                detalleFactura.setIdDetalleFactura(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(DetalleFactura detalleFactura) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update detalle_factura set cantidad=?,valor_parcial=?,id_producto=?,id_factura=? where id_detalle_factura=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, detalleFactura.getCantidad());
            sentencia.setObject(i++, detalleFactura.getValorParcial());
            sentencia.setObject(i++, detalleFactura.getProducto().getIdProducto());
            sentencia.setObject(i++, detalleFactura.getFactura().getIdFactura());
            sentencia.setObject(i++, detalleFactura.getIdDetalleFactura());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<DetalleFactura> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<DetalleFactura> lista = new ArrayList<>();
        try {

            String sql = "select * from detalle_factura";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getDetalleFactura(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public DetalleFactura consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        DetalleFactura obj = null;
        try {

            String sql = "select * from detalle_factura where id_detalle_factura=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getDetalleFactura(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static DetalleFactura getDetalleFactura(ResultSet rs) throws SQLException {
        DetalleFactura detalleFactura = new DetalleFactura();
        detalleFactura.setIdDetalleFactura(rs.getLong("id_detalle_factura"));
        detalleFactura.setCantidad(rs.getLong("cantidad"));
        detalleFactura.setValorParcial(rs.getLong("valor_parcial"));
        detalleFactura.setProducto(new Producto(rs.getLong("id_producto")));
        detalleFactura.setFactura(new Factura(rs.getLong("id_factura")));
        return detalleFactura;
    }

}
