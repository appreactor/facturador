package co.movistar.facturador.persistencia.dao.crud;

import co.movistar.facturador.persistencia.basedatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.facturador.persistencia.entidades.Rol;

public class RolCrud implements IGenericoDao<Rol> {

    protected final int ID = 1;
    protected Connection cnn;

    public RolCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Rol rol) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into rol(nombre,estado) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, rol.getNombre());
            sentencia.setObject(i++, rol.getEstado());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                rol.setIdRol(rs.getLong(ID));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Rol rol) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update rol set nombre=?,estado=? where id_rol=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, rol.getNombre());
            sentencia.setObject(i++, rol.getEstado());
            sentencia.setObject(i++, rol.getIdRol());

            sentencia.executeUpdate();
        } finally {
            Conexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Rol> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Rol> lista = new ArrayList<>();
        try {

            String sql = "select * from rol";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getRol(rs));
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Rol consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Rol obj = null;
        try {

            String sql = "select * from rol where id_rol=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getRol(rs);
            }
        } finally {
            Conexion.desconectar(sentencia);
        }
        return obj;
    }

    public static Rol getRol(ResultSet rs) throws SQLException {
        Rol rol = new Rol();
        rol.setIdRol(rs.getLong("id_rol"));
        rol.setNombre(rs.getString("nombre"));
        rol.setEstado(rs.getBoolean("estado"));

        return rol;
    }


}
