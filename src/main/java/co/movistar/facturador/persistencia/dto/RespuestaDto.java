/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.persistencia.dto;

import co.movistar.facturador.negocio.constantes.EMensajes;
import co.movistar.facturador.negocio.excepciones.FacturadorException;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author Lord_Nightmare
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RespuestaDto<T> implements Serializable{
    
    private int codigo;
    private String mensaje;
    private T datos;

    public RespuestaDto() {
    }
    
    public RespuestaDto(EMensajes mensaje){
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getMensaje();
    }
    
    public RespuestaDto(FacturadorException e){
        this.codigo = e.getCodigo();
        this.mensaje = e.getDescripcion();
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the datos
     */
    public T getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(T datos) {
        this.datos = datos;
    }
    
    
    
}
