/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.facturador.persistencia.entidades;

import java.util.Map;

/**
 *
 * @author Lord_Nightmare
 */
public class Entidad {

    private Map<String, Object> datosAdicionales;

    /**
     * @return the datos
     */
    public Map<String, Object> getDatosAdicionales() {
        return datosAdicionales;
    }

    /**
     * @param datosAdicionales the datos to set
     */
    public void setDatosAdicionales(Map<String, Object> datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }

}
