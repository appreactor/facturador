package co.movistar.facturador.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Fabian
 */
public class Rol implements Serializable {

    private Long idRol;
    private String nombre;
    private Boolean estado;

    public Rol() {
    }

    public Rol(Long idRol) {
        this.idRol = idRol;
    }

    public Long getIdRol() {
        return idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
