package co.movistar.facturador.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Fabian
 */
public class TipoCliente implements Serializable {

    private Long idTipoCliente;
    private String nombre;
    private Boolean estado;

    public TipoCliente() {
    }

    public TipoCliente(Long idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public Long getIdTipoCliente() {
        return idTipoCliente;
    }

    public void setIdTipoCliente(Long idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
