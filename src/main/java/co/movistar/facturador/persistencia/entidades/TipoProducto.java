package co.movistar.facturador.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Fabian
 */
public class TipoProducto implements Serializable {

    private Long idTipoProducto;
    private String nombre;
    private Boolean estado;

    public TipoProducto() {
    }

    public TipoProducto(Long idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public Long getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Long idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
